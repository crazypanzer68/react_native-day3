/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
// import App from './App';
// import App from './Screen1'
// import App from './Screen2'
import App from './Router'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
