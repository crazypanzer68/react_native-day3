import React, { Component } from 'react';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import App from './App'
import Profile from './Profile'
import Screen1 from './Screen1'
import Screen2 from './Screen2'

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path='/' component={App} />
                    <Route exact path='/profile' component={Profile} />
                    <Route exact path='/screen1' component={Screen1} />
                    <Route exact path='/screen2' component={Screen2} />
                    <Redirect to='/' />
                </Switch>
            </NativeRouter>
        )
    }
}

export default Router