import React, { Component } from 'react';
import { Text, View, Button } from 'react-native';

class Screen2 extends Component {
    goToscreen1 = () => {
        this.props.history.push('/screen1', {
            mynumber: 13
        })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'yellow' }}>
                <Text style={{ color: 'purple' }}>Screen2</Text>
                <Button title="Go to screen1" onPress={this.goToscreen1} />
            </View>
        )
    }
}

export default Screen2