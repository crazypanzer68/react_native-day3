import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Alert, TouchableHighlight, TextInput, Image } from 'react-native';
import { Link } from 'react-router-native'

class Profile extends Component {
    UNSAFE_componentWillMount() {
        console.log(this.props)
    }
    goToscreen1 = () => {
        this.props.history.push('/screen1')
    }
    showImage(image) {
        this.setState({ images: image })
    }

    render() {
        return (
            <View style={styles.container}>
                <Button title="<" onPress={this.goToscreen1}></Button>
                <View style={styles.container}>
                    <Text style={[styles.text]}>Username : {this.props.location.state.Username}</Text>
                    <Text style={[styles.text]}>First Name</Text>
                    <Text style={[styles.text]}>Last Name</Text>
                    <Text style={[styles.text]}>Welcome to React Native!</Text>
                </View>
            </View>
        )
    }
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    modal: {
        marginTop: 30,
        width: '80%',
        height: '80%',
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        textAlignVertical: 'center',
        textAlign: 'center',
        fontSize: 25
    },
    image: {
        width: 250,
        height: 240
    }
})